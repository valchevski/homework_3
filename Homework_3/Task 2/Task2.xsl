<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    <xsl:key name="element" match="node() | @*" use="name(.)"/>
    <xsl:variable name="quote">'</xsl:variable>

    <!-- from the node-set of nodes and attributes, that have the same name,
         choose the 1-st one, by comparing it's generated id with generated id of the whole node-set; 
         applying template only for this 1-st element to avoid duplicates; -->
    <xsl:template match="/">
        <xsl:apply-templates
            select="
                descendant::node()[generate-id(.) = generate-id(key('element', name(.)))] |
                descendant::node()/@*[generate-id(.) = generate-id(key('element', name(.)))]"/>
        <xsl:apply-templates select="descendant::text()"/>
    </xsl:template>

    <xsl:template match="node() | @*">
        <xsl:variable name="count" select="count(key('element', name(.)))"/>
        <xsl:value-of select="concat('Node ', $quote, name(.), $quote, ' found ', $count)"/>
        <xsl:choose>
            <xsl:when test="$count = 1"> time. &#xa;</xsl:when>
            <xsl:otherwise> times. &#xa;</xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="text()"/>

</xsl:stylesheet>
