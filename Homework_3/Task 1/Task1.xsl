<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:xalan="http://xml.apache.org/xslt" exclude-result-prefixes=" xalan">
    <xsl:output method="xml" omit-xml-declaration="yes" indent="yes" xalan:indent-amount="4"/>
    <xsl:key name="person" match="item" use="substring(@Name, 1, 1)"/>
    
    <!-- creates "list" element; 
         from the node-set of the elements, that have the same 1-st letter in the value of attribute @Name,
         choose the 1-st one, by comparing it's generated id with generated id of the whole node-set; 
         applying template only for this 1-st element to avoid duplicates;
         sort by @Name attribute to arrange output in alphabetic order -->
    <xsl:template match="list">
        <list>
            <xsl:apply-templates
                select="item[generate-id(.) = generate-id(key('person', substring(@Name, 1, 1)))]">
                <xsl:sort select="@Name"/>
            </xsl:apply-templates>
        </list>
    </xsl:template>
    
    <!-- creates node for every unique letter;
         for each element, that have the same 1-st letter in the value of attribute @Name, 
         creates node "name" and puts the value of @Name attribute as text node;
         sort by @Name attribute to arrange output in alphabetic order -->
    <xsl:template match="item">
        <capital value="{substring(@Name,1,1)}">
            <xsl:for-each select="key('person', substring(@Name, 1, 1))">
                <xsl:sort select="@Name"/>
                <name>
                    <xsl:value-of select="@Name"/>
                </name>
            </xsl:for-each>
        </capital>
    </xsl:template>
</xsl:stylesheet>
